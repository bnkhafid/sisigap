<?= $this->extend('layout/admin') ?>

<?= $this->section('content') ?>

<?php $imagestyle="border: 10px solid white;
                   border-radius: 20px;
                   height: 300px;
                   width: 300px;
                   background-repeat: no-repeat;
                   background-position: center;
                   background-size: cover;" ?>



<section class="content-header">
    <div class="container-fluid">
    <div class="row mb-2">
        <div class="col-sm-6">
        <h1>Detail</h1>
        </div>
        <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Admin</a></li>
            <li class="breadcrumb-item">PPA</li>
            <li class="breadcrumb-item active">Detail</li>
        </ol>
        </div>
    </div>
    </div>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-body">
                    <?= session()->getFlashdata('info') ?>
                    <table class="table table-striped">
                        <tr>
                            <th>Status</th>
                            <td>
                                <span class="badge <?php if($laporan['status'] == 'Sedang Diproses'){ echo 'alert-warning'; }
                                elseif($laporan['status'] == 'Tidak Valid'){ echo 'alert-danger'; }
                                else{ echo 'alert-success'; } ?>"><?= $laporan['status'] ?></span>
                                <a class="badge btn-primary" href="#" onclick="modalubah()" >Ubah</a>
                            </td>
                        </tr>
                        <tr>
                            <th>Kode Laporan </th>
                            <td><?= $laporan['kode_laporanppa'] ?></td>
                        </tr>
                        <tr>
                            <th>Nama Pelapor </th>
                            <td><?= $laporan['nama_pelapor'] ?></td>
                        </tr>
                        <tr>
                            <th>NIK Pelapor </th>
                            <td><?= $laporan['ktp_pelapor'] ?></td>
                        </tr>
                        <tr>
                            <th>WA Pelapor </th>
                            <td><?= $laporan['wa_pelapor'] ?></td>
                        </tr>
                        <tr>
                            <th>HP Pelapor </th>
                            <td><?= $laporan['hp_pelapor'] ?></td>
                        </tr>
                        <tr>
                            <th>Nama Korban </th>
                            <td><?= $laporan['nama_korban'] ?></td>
                        </tr>
                        <tr>
                            <th>Perkiraan Usia </th>
                            <td><?= $laporan['perkiraan_usia_korban'] ?></td>
                        </tr>
                        <tr>
                            <th>Alamat Kejadian </th>
                            <td><?= $laporan['alamat_kejadian'] ?></td>
                        </tr>
                        <tr>
                            <th>Kronologis Kejadian </th>
                            <td><?= $laporan['kronologi_kejadian'] ?></td>
                        </tr>
                    </table><hr>
                    <div class="row">
                        <div style="background: url('<?= base_url('/uploads/'.$laporan['bukti_foto1'].'') ?>'); <?= $imagestyle ?>" class="col-sm-4"></div>
                        <div style="background: url('<?= base_url('/uploads/'.$laporan['bukti_foto2'].'') ?>'); <?= $imagestyle ?>" class="col-sm-4"></div>
                        <div style="background: url('<?= base_url('/uploads/'.$laporan['bukti_foto3'].'') ?>'); <?= $imagestyle ?>" class="col-sm-4"></div>
                    </div><hr>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header bg-primary">Kotak Pesan</div>
                                <div class="card-body">
                                   <?php foreach($pesan as $pesan) : ?>
                                        <div class="alert <?php if($pesan['sender'] == 'Admin'){ echo "alert-warning"; }else{ echo "alert-success"; } ?>" role="alert">
                                            <b><?= $pesan['sender'] ?> : </b><span>"<?= $pesan['pesan'] ?>"</span><small class="float-right"><?= $pesan['created_at'] ?></small>
                                        </div>
                                   <?php endforeach; ?>
                                   
                                   <form action="<?= base_url() ?>/admin/ppa/kirim_pesan/<?= $laporan['id'] ?>" method="post">
                                    <div class="row">
                                            <input required type="text" class="form-control col-sm-10" name="pesan" id="pesan" placeholder="Ketik Pesan">
                                            <button <?php if($laporan['status'] != 'Sedang Diproses'){ echo 'disabled'; } ?> type="submit" class="col-sm-2 btn btn-primary">Kirim</button>
                                    </div>
                                   </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <a href="<?= base_url() ?>/admin/ppa" class="btn btn-sm btn-danger float-right">Kembali</a>
                </div>
            </div>
        </div>
    </div>
</section>



<div class="modal fade" tabindex="-1" id="modalubah">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Pilih Status Untuk Mengubah!</h5>
            </div>
            <form action="<?= base_url() ?>/admin/ppa/update/<?= $laporan['id'] ?>" method="post">
            <div class="modal-body">
               <select name="status" id="" class="form-control">
                    <option value="Sedang Diproses">Sedang Diproses</option>
                    <option value="Tidak Valid">Tidak Valid</option>
                    <option value="Laporan Selesai">Laporan Selesai</option>
               </select>
            </div>
            <div class="modal-footer bg-light">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-default">Yakin & Simpan!</button>
            </div>
            </form>
        </div>
    </div>
</div>


<script>
let img = document.querySelectorAll("#bukti_foto");
img[0].style.width = '100px';
img[1].style.width = '100px';
img[2].style.width = '100px';


function modalubah(id){
    $('#modalubah').modal('show');
}
</script>


<?= $this->endSection() ?>