<?= $this->extend('layout/admin') ?>

<?= $this->section('content') ?>

<section class="content-header">
    <div class="container-fluid">
    <div class="row mb-2">
        <div class="col-sm-6">
        <h1>Layanan Perempuan Dan Anak</h1>
        </div>
        <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Admin</a></li>
            <li class="breadcrumb-item active">PPA</li>
        </ol>
        </div>
    </div>
    </div>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <h5 class="float-left">Daftar Laporan</h5>
                    </div>
                    <div class="card-body">
                    <?= session()->getFlashdata('info') ?>
                        <table id="datatable" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Kode Laporan</th>
                                    <th>Nama Pelapor</th>
                                    <th>NIK Pelapor</th>
                                    <th>Wa Pelapor</th>
                                    <th>Nama Korban</th>
                                    <th>Status</th>
                                    <th style="width:100px;">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $no = 1; foreach($laporan as $laporan) : ?>
                                <tr>
                                    <td><?= $no++ ?></td>
                                    <td><?= $laporan['kode_laporanppa'] ?></td>
                                    <td><?= $laporan['nama_pelapor'] ?></td>
                                    <td><?= $laporan['ktp_pelapor'] ?></td>
                                    <td><?= $laporan['hp_pelapor'] ?></td>
                                    <td><?= $laporan['nama_korban'] ?></td>
                                    <td><span class="badge <?php if($laporan['status'] == 'Sedang Diproses'){ echo 'alert-warning'; }
                                                                 elseif($laporan['status'] == 'Tidak Valid'){ echo 'alert-danger'; }
                                                                 else{ echo 'alert-success'; } ?>"><?= $laporan['status'] ?></span></td>
                                    <td>
                                        <a style="width:100px" href="<?= base_url() ?>/admin/ppa/detail/<?= $laporan['id'] ?>" class="badge btn-primary"><i class="far fa-eye"></i> Lihat</a><br>
                                        <a style="width:100px" href="<?= base_url() ?>/admin/ppa/hapus/<?= $laporan['id'] ?>" <?php if($laporan['status'] != 'Tidak Valid' ){ echo 'hidden'; } ?> class="badge btn-danger"><i class="fas fa-trash"></i> Hapus</a>
                                    </td>
                                </tr>
                            <?php endforeach;  ?>    
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?= $this->endSection() ?>