<?= $this->extend('layout/front') ?>

<?= $this->section('content') ?>
<div class="container">
    <div class="row">
        <div class="col">
            <div class="mt-5">
                <div class="card-body">
                    <div class="d-grid col-10 gap-2 mx-auto">
                        <?= session()->getFlashdata('info') ?>
                        <div class="accordion" id="accordionExample">
                        <form id="form" action="<?= base_url() ?>/layananumum/laporkan/<?= $laporan ?>" method="post" enctype="multipart/form-data">
                        </div>
                        <div class="mb-3">
                            <label class="form-label">Jenis Laporan</label>
                            <input type="text" readonly required id="kategori_aduan" value="<?= $laporan ?>" name="kategori_aduan" class="form-control" placeholder="Nama">
                        </div>
                        <div class="mb-3">
                            <label class="form-label">Nama Pelapor</label>
                            <input type="text" required id="nama_pelapor" name="nama_pelapor" class="form-control" placeholder="Nama">
                        </div>
                        
                        <div class="mb-3">
                            <label class="form-label">No. HP</label>
                            <input type="number" required id="no_hp" name="no_hp" class="form-control" placeholder="No. HP">
                        </div>
                        <div class="mb-3">
                            <label class="form-label">Alamat Pelapor</label>
                            <input type="text" required id="alamat_pelapor" name="alamat_pelapor" class="form-control" placeholder="Alamat Pelapor">
                        </div>
                        <div class="mb-3">
                            <label class="form-label">Alamat Kejadian</label>
                            <select name="kec_tkp" id="" class="form-control">
                                <option value="0">-- Pilih Kecamatan --</option>  
                                <option value="Balongbendo">Balongbendo</option>   
                                <option value="Krian">Krian</option>  	
                                <option value="Taman">Taman</option>  
                                <option value="Buduran">Buduran</option>  	
                                <option value="Porong">Porong</option>  	
                                <option value="Tanggulangin">Tanggulangin</option>  
                                <option value="Candi">Candi</option>  	
                                <option value="Prambon">Prambon</option>  	
                                <option value="Tarik">Tarik</option>  
                                <option value="Gedangan">Gedangan</option>  	
                                <option value="Sedati">Sedati</option>  	
                                <option value="Tulangan">Tulangan</option>  
                                <option value="Jabon">Jabon</option>  	
                                <option value="Sidoarjo">Sidoarjo</option>  	
                                <option value="Waru">Waru</option>  
                                <option value="Krembung">Krembung</option>  	
                                <option value="Sukodono">Sukodono</option>  	
                                <option value="Wonoayu">Wonoayu</option>  
                            </select>
                            <input type="text" required id="alamat_tkp" name="alamat_tkp" class="form-control" placeholder="Alamat">
                        </div>
                        <a href="<?= base_url() ?>/layananumum" class="btn btn-danger">Batal</a>
                        <button onclick="modalCek()" class="btn btn-primary mb-5">Laporkan</button>
                    </div>
                    </form> 
                </div>
            </div>
        </div>
    </div>
</div>

<?= $this->endSection() ?>