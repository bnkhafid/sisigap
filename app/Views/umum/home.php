<?= $this->extend('layout/front') ?>

<?= $this->section('content') ?>

<div class="container">
    <div class="row">
        <div class="col">
            <div class="mt-5">
                <div class="card-body">
                    <div class="d-grid col-10 gap-2 mx-auto">
                        <a href="<?= base_url() ?>/layananumum/laporkan/Kebakaran" class="btn btn-warning"> <b> kebakaran</b></a>
                        <a href="<?= base_url() ?>/layananumum/laporkan/Tawuran" class="btn btn-warning"> <b>Tawuran</b> </a>
                        <a href="<?= base_url() ?>/layananumum/laporkan/Balap%20liar" class="btn btn-warning"> <b> Balap Liar</b></a>
                        <a href="<?= base_url() ?>/layananumum/laporkan/Premanisme" class="btn btn-warning"> <b>Premanisme</b> </a>
                        <a href="<?= base_url() ?>/layananumum/laporkan/Kriminal" class="btn btn-warning"> <b>Kriminal</b> </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>




<?= $this->endSection() ?>