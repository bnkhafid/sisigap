<?= $this->extend('layout/front') ?>

<?= $this->section('content') ?>

<div class="container">
    <div class="row">
        <div class="col">
            <div class="mt-5">
                <div class="card-body">
                    <div class="d-grid col-10 gap-2 mx-auto">
                        <a href="<?= base_url() ?>/layananppa/cek" class="btn btn-lg btn-warning"> <b> Cek Status Laporan</b></a>
                        <a href="<?= base_url() ?>/layananppa/new" class="btn btn-lg btn-warning"> <b>Buat Laporan Baru</b> </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>




<?= $this->endSection() ?>