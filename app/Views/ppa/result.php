<?= $this->extend('layout/front') ?>

<?= $this->section('content') ?>

<div class="container ">
    <div class="row">
        <div class="col">
            <div class="mt-5">
                <div class="card-body">
                    <h3>Kode Laporan : <?= $laporan['kode_laporanppa'] ?></h3>
                    <p>Harap Simpan Kode Diatas Baik Baik!</p>
                    <a href="<?= base_url() ?>/layananppa" class="btn btn-primary btn-sm mb-2" >Kembali</a>
                    <a href="<?= base_url() ?>/layananppa/exportpdf/<?= $laporan['kode_laporanppa'] ?>" class="btn btn-danger btn-sm mb-2" >Download Pdf</a>
                    <table class="table table-striped">
                        <tr>
                            <th>Nama Pelapor </th>
                            <td><?= $laporan['nama_pelapor'] ?></td>
                        </tr>
                        <tr>
                            <th>NIK Pelapor </th>
                            <td><?= $laporan['ktp_pelapor'] ?></td>
                        </tr>
                        <tr>
                            <th>WA Pelapor </th>
                            <td><?= $laporan['wa_pelapor'] ?></td>
                        </tr>
                        <tr>
                            <th>HP Pelapor </th>
                            <td><?= $laporan['hp_pelapor'] ?></td>
                        </tr>
                        <tr>
                            <th>Nama Korban </th>
                            <td><?= $laporan['nama_korban'] ?></td>
                        </tr>
                        <tr>
                            <th>Perkiraan Usia </th>
                            <td><?= $laporan['perkiraan_usia_korban'] ?></td>
                        </tr>
                        <tr>
                            <th>Alamat Kejadian </th>
                            <td><?= $laporan['alamat_kejadian'] ?></td>
                        </tr>
                        <tr>
                            <th>Kronologi </th>
                            <td><?= $laporan['kronologi_kejadian'] ?></td>
                        </tr>
                        <tr>
                            <th>Foto </th>
                            <td>
                                <img id="bukti_foto" src="<?= base_url() ?>/uploads/<?= $laporan['bukti_foto1'] ?>" alt="">
                                <img id="bukti_foto" src="<?= base_url() ?>/uploads/<?= $laporan['bukti_foto2'] ?>" alt="">
                                <img id="bukti_foto" src="<?= base_url() ?>/uploads/<?= $laporan['bukti_foto3'] ?>" alt="">
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
let img = document.querySelectorAll("#bukti_foto");
img[0].style.width = '100px';
img[1].style.width = '100px';
img[2].style.width = '100px';
</script>




<?= $this->endSection() ?>