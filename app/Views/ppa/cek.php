<?= $this->extend('layout/front') ?>

<?= $this->section('content') ?>

<div class="container">
    <div class="row">
        <div class="col">
            <div class="mt-5">
                <div class="card-body">
                    <form action="<?= base_url() ?>/layananppa/cari" method="post">
                        <div class="d-grid col-10 gap-2 mx-auto">
                            <?= session()->getFlashdata('info') ?>
                            <input class="form-control" name="cari" type="text" placeholder="Masukkan Kode Laporan / NIK">
                            <button type="submit" class="btn btn-sm btn-primary">Cari</button>
                            <a href="<?= base_url() ?>/layananppa" class="btn btn-sm btn-danger">Batal</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>




<?= $this->endSection() ?>