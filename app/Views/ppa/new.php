<?= $this->extend('layout/front') ?>

<?= $this->section('content') ?>
<div class="container">
    <div class="row">
        <div class="col">
            <div class="mt-5">
                <div class="card-body">
                    <div class="d-grid col-10 gap-2 mx-auto">
                        <?= session()->getFlashdata('info') ?>
                        <div class="accordion" id="accordionExample">
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="headingTwo">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    Petunjuk Cara Pengisian
                                </button>
                                </h2>
                                <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                                <div class="accordion-body">
                                    <strong>This is the second item's accordion body.</strong> It is hidden by default, until the collapse plugin adds the appropriate classes that we use to style each element. These classes control the overall appearance, as well as the showing and hiding via CSS transitions. You can modify any of this with custom CSS or overriding our default variables. It's also worth noting that just about any HTML can go within the <code>.accordion-body</code>, though the transition does limit overflow.
                                </div>
                                </div>
                            </div>
                        <form id="form" action="<?= base_url() ?>/layananppa/create" method="post" enctype="multipart/form-data">
                        </div>
                        <div class="mb-3">
                            <label class="form-label">Nama Pelapor</label>
                            <input type="text" required id="nama_pelapor" name="nama_pelapor" class="form-control" placeholder="Nama">
                        </div>
                        <div class="mb-3">
                            <label class="form-label">NIK Pelapor</label>
                            <input type="number" required id="nik_pelapor" name="nik_pelapor" class="form-control" placeholder="NIK">
                        </div>
                        <div class="mb-3">
                            <label class="form-label">No. WA</label>
                            <input type="number" required id="wa_pelapor" name="wa_pelapor" class="form-control" placeholder="No. WA">
                        </div>
                        <div class="mb-3">
                            <label class="form-label">No. HP</label>
                            <input type="number" required id="hp_pelapor" name="hp_pelapor" class="form-control" placeholder="No. HP">
                        </div>
                        <div class="mb-3">
                            <label class="form-label">Nama Korban</label>
                            <input type="text" required id="nama_korban" name="nama_korban" class="form-control" placeholder="Nama Korban">
                        </div>
                        <div class="mb-3">
                            <label class="form-label">Perkiraan Usia Korban</label>
                            <input type="number" onKeyPress="if(this.value.length==3) return false;" required id="usia_korban" name="usia_korban" class="form-control" placeholder="Perkiraan Usia Korban">
                        </div>
                        <div class="mb-3">
                            <label class="form-label">Alamat Kejadian</label>
                            <input type="text" required id="alamat_kejadian" name="alamat_kejadian" class="form-control" placeholder="Alamat Kejadian">
                        </div>
                        <div class="mb-3">
                            <label class="form-label">Kronologi Kejadian</label>
                            <textarea required id="kronologi_kejadian" name="kronologi_kejadian" class="form-control" rows="3"></textarea>
                        </div>
                        <div class="mb-3">
                            <img style="width:120px;" hidden="true" id="previewImg1"><br>
                            <label class="form-label">Bukti Foto 1</label>
                            <input type="file"  onchange="previewFile1(this);" required id="foto1" name="foto1" class="form-control">
                        </div>
                        <div class="mb-3">
                            <img style="width:120px;" hidden="true" id="previewImg2"><br>
                            <label class="form-label">Bukti Foto 2</label>
                            <input type="file" onchange="previewFile2(this);" id="foto2" name="foto2" class="form-control">
                        </div>
                        <div class="mb-3">
                            <img style="width:120px;" hidden="true" id="previewImg3"><br>
                            <label class="form-label">Bukti Foto 3</label>
                            <input type="file" onchange="previewFile3(this);" id="foto3" name="foto3" class="form-control">
                        </div>
                        <a href="<?= base_url() ?>/layananppa" class="btn btn-danger">Batal</a>
                        <div onclick="modalCek()" class="btn btn-primary mb-5">Cek Laporan</div>
                        
                    </div>
                    </form> 
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" id="modal-cek">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                <em class="icon ni ni-cross"></em>
            </a>
            <div class="modal-header">
                <h5 class="modal-title">Cek Data Anda!</h5>
            </div>
            <div class="modal-body">

                <table class="table">
                    <tr>
                        <td>Nama Pelapor</td>
                        <td>:</td>
                        <td id="pelapor"></td>
                    </tr>
                    <tr>
                        <td>NIK Pelapor</td>
                        <td>:</td>
                        <td id="nik"></td>
                    </tr>
                    <tr>
                        <td>WA Pelapor</td>
                        <td>:</td>
                        <td id="wa"></td>
                    </tr>
                    <tr>
                        <td>HP Pelapor</td>
                        <td>:</td>
                        <td id="hp"></td>
                    </tr>
                    <tr>
                        <td>Nama Korban</td>
                        <td>:</td>
                        <td id="korban"></td>
                    </tr>
                    <tr>
                        <td>Usia Korban</td>
                        <td>:</td>
                        <td id="usia"></td>
                    </tr>
                    <tr>
                        <td>Alamat Kejadian</td>
                        <td>:</td>
                        <td id="alamat"></td>
                    </tr>
                    <tr>
                        <td>Kronologi</td>
                        <td>:</td>
                        <td id="kronologi"></td>
                    </tr>
                </table>
            </div>
            <div class="modal-footer bg-light">
                <button onclick="submit()" class="btn btn-success">Kirim Laporan!</button>
            </div>
        </div>
    </div>
</div>


<script>
function submit(){
    document.getElementById('form').submit();
}
function previewFile1(input){
    var file = $("input[name=foto1]").get(0).files[0];
    if(file){
        var reader = new FileReader();
        reader.onload = function(){
            $("#previewImg1").attr("hidden", false);
            $("#previewImg1").attr("src", reader.result);
        }
        reader.readAsDataURL(file);
    }
}
function previewFile2(input){
    var file = $("input[name=foto2]").get(0).files[0];
    if(file){
        var reader = new FileReader();
        reader.onload = function(){
            $("#previewImg2").attr("hidden", false);
            $("#previewImg2").attr("src", reader.result);
        }
        reader.readAsDataURL(file);
    }
}
function previewFile3(input){
    var file = $("input[name=foto3]").get(0).files[0];
    if(file){
        var reader = new FileReader();
        reader.onload = function(){
            $("#previewImg3").attr("hidden", false);
            $("#previewImg3").attr("src", reader.result);
        }
        reader.readAsDataURL(file);
    }
}

function modalCek(){
    let nama = $('#nama_pelapor').val();
    let nik = $('#nik_pelapor').val();
    let wa = $('#wa_pelapor').val();
    let hp = $('#hp_pelapor').val();
    let korban = $('#nama_korban').val();
    let usia = $('#usia_korban').val();
    let alamat = $('#alamat_kejadian').val();
    let kronologi = $('#kronologi_kejadian').val();
    let foto1 = $('#foto1').val();
    if(nama != '' && nik != '' && wa != '' && hp != '' && korban != '' && usia != '' && alamat != '' && kronologi != '' && foto1 != ''){
        $('#pelapor').html(nama);
        $('#nik').html(nik);
        $('#wa').html(wa);
        $('#hp').html(hp);
        $('#korban').html(korban);
        $('#usia').html(usia);
        $('#alamat').html(alamat);
        $('#kronologi').html(kronologi);
        $('#modal-cek').modal('show');
    }else{
        alert('Masukkan Semua Data Terlebih Dahulu');
    }
    
    console.log(foto1);
}

</script>

<?= $this->endSection() ?>