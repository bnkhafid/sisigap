<?= $this->extend('layout/front') ?>

<?= $this->section('content') ?>

<div class="container ">
    <div class="row">
        <div class="col">
            <div class="mt-5">
                <div class="card-body text-center">
                    <h2>KODE LAPORAN : <?= $laporan['kode_laporanppa'] ?></h2>
                    <p>(Harap Simpan Kode Ini Baik Baik! Jangan Sampai Hilang!)</p>
                    <table class="table">
                        <tr>
                            <th>Nama Pelapor </th>
                            <td>:</td>
                            <td><?= $laporan['nama_pelapor'] ?></td>
                        </tr>
                        <tr>
                            <th>NIK Pelapor </th>
                            <td>:</td>
                            <td><?= $laporan['ktp_pelapor'] ?></td>
                        </tr>
                        <tr>
                            <th>WA Pelapor </th>
                            <td>:</td>
                            <td><?= $laporan['wa_pelapor'] ?></td>
                        </tr>
                        <tr>
                            <th>HP Pelapor </th>
                            <td>:</td>
                            <td><?= $laporan['hp_pelapor'] ?></td>
                        </tr>
                        <tr>
                            <th>Nama Korban </th>
                            <td>:</td>
                            <td><?= $laporan['nama_korban'] ?></td>
                        </tr>
                        <tr>
                            <th>Perkiraan Usia </th>
                            <td>:</td>
                            <td><?= $laporan['perkiraan_usia_korban'] ?></td>
                        </tr>
                        <tr>
                            <th>Alamat Kejadian </th>
                            <td>:</td>
                            <td><?= $laporan['alamat_kejadian'] ?></td>
                        </tr>
                        <tr>
                            <th>Kronologis Kejadian </th>
                            <td>:</td>
                            <td><?= $laporan['kronologi_kejadian'] ?></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<?= $this->endSection() ?>