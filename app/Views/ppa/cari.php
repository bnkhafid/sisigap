<?= $this->extend('layout/front') ?>

<?= $this->section('content') ?>
<?php $imagestyle="border: 3px solid white;
                   border-radius: 20px;
                   height: 100px;
                   float: left;
                   width: 100px;
                   background-repeat: no-repeat;
                   background-position: center;
                   background-size: cover;" ?>

<div class="container ">
    <div class="row">
        <div class="col">
            <div class="mt-2 mb-5">
                <div class="card-body">
                    <h3>Kode Laporan : <?= $laporan['kode_laporanppa'] ?></h3>
                    <div class="row">
                        <div class="col-sm-6">
                            <table class="table table-striped">
                                <tr>
                                    <th>Status</th>
                                    <th><span class="badge <?php if($laporan['status'] == 'Sedang Diproses'){ echo 'alert-warning'; }
                                                                 elseif($laporan['status'] == 'Tidak Valid'){ echo 'alert-danger'; }
                                                                 else{ echo 'alert-success'; } ?>"><?= $laporan['status'] ?></span></th>
                                </tr>
                                <tr>
                                    <th>Nama Pelapor </th>
                                    <td><?= $laporan['nama_pelapor'] ?></td>
                                </tr>
                                <tr>
                                    <th>NIK Pelapor </th>
                                    <td><?= $laporan['ktp_pelapor'] ?></td>
                                </tr>
                                <tr>
                                    <th>WA Pelapor </th>
                                    <td><?= $laporan['wa_pelapor'] ?></td>
                                </tr>
                                <tr>
                                    <th>HP Pelapor </th>
                                    <td><?= $laporan['hp_pelapor'] ?></td>
                                </tr>
                                <tr>
                                    <th>Nama Korban </th>
                                    <td><?= $laporan['nama_korban'] ?></td>
                                </tr>
                                <tr>
                                    <th>Perkiraan Usia </th>
                                    <td><?= $laporan['perkiraan_usia_korban'] ?></td>
                                </tr>
                                <tr>
                                    <th>Alamat Kejadian </th>
                                    <td><?= $laporan['alamat_kejadian'] ?></td>
                                </tr>
                                <tr>
                                    <th>Kronologi </th>
                                    <td><?= $laporan['kronologi_kejadian'] ?></td>
                                </tr>
                                <tr>
                                    <th>Foto </th>
                                    <td>
                                        <div style="background: url('<?= base_url('/uploads/'.$laporan['bukti_foto1'].'') ?>'); <?= $imagestyle ?>"></div>
                                        <div style="background: url('<?= base_url('/uploads/'.$laporan['bukti_foto2'].'') ?>'); <?= $imagestyle ?>"></div>
                                        <div style="background: url('<?= base_url('/uploads/'.$laporan['bukti_foto3'].'') ?>'); <?= $imagestyle ?>"></div>
                                    </td>
                                </tr>
                            </table><hr>
                        </div>
                        <div class="col-sm-6">
                            <?php foreach($catatan as $pesan) : ?>
                                <div class="alert <?php if($pesan['sender'] == 'Admin'){ echo "alert-success"; }else{ echo "alert-secondary"; } ?>" role="alert">
                                    <b><?= $pesan['sender'] ?> : </b><span>"<?= $pesan['pesan'] ?>"</span><small class="float-right"><?= $pesan['created_at'] ?></small>
                                </div>
                            <?php endforeach; ?>
                            
                            <form action="<?= base_url() ?>/admin/ppa/kirim_pesan/<?= $laporan['id'] ?>" method="post">
                            <div class="row">
                                    <input required type="text" class="form-control col-sm-10" name="pesan" id="pesan" placeholder="Ketik Pesan">
                                    <button <?php if($laporan['status'] != 'Sedang Diproses'){ echo 'disabled'; } ?> type="submit" class="col-sm-2 btn btn-primary">Kirim</button>
                            </div>
                            </form>
                        </div>
                    </div>
                   
                    
                </div>      
                <a href="<?= base_url() ?>/layananppa/cek" class="btn btn-warning btn-sm mb-2" >Kembali</a>
                <a href="<?= base_url() ?>/layananppa/exportpdf/<?= $laporan['kode_laporanppa'] ?>" class="btn btn-danger btn-sm mb-2" >Download Pdf</a>
                <a href="<?= base_url() ?>/layananppa/edit/<?= $laporan['id'] ?>" class="btn btn-primary btn-sm mb-2" >Edit</a>  
            </div>
        </div>
    </div>
</div>

<script>
let img = document.querySelectorAll("#bukti_foto");
img[0].style.width = '100px';
img[1].style.width = '100px';
img[2].style.width = '100px';
</script>




<?= $this->endSection() ?>