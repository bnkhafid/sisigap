<?php

namespace App\Controllers;
use App\Models\Laporanppa_m;
use App\Models\Laporanppacatatan_m;

class LayananPPA extends BaseController
{
	public function __construct()
    {
        // parent::__construct();
		$this->laporanppa = new Laporanppa_m();
		$this->catatanlaporanppa = new Laporanppacatatan_m();
    }
	public function index()
	{
		return view('ppa/home');
	}
	public function cek(){
		return view('ppa/cek');
	}
	public function new(){
		return view('ppa/new');
	}
	public function create(){
		
		$foto1 = $this->request->getFile('foto1');
		$foto2 = $this->request->getFile('foto2');
		$foto3 = $this->request->getFile('foto3');

		$count = 0;
		$rules = [
			'foto1' => 'ext_in[foto1,png,jpg,jpeg]',
			'foto2' => 'ext_in[foto2,png,jpg,jpeg]',
			'foto3' => 'ext_in[foto3,png,jpg,jpeg]',
		];

		if($this->validate($rules) == true){

			$name1 = '';
			$name2 = '';
			$name3 = '';
			if($foto1->isValid()){
				$name1 = $foto1->getRandomName();
	           	$foto1->move('uploads', $name1);
			}else{
				$name1 = 'no-image.png';
			}

			if($foto2->isValid()){
				$name2 = $foto2->getRandomName();
	           	$foto2->move('uploads', $name2);
			}else{
				$name2 = 'no-image.png';
			}

			if($foto3->isValid()){
				$name3 = $foto3->getRandomName();
	           	$foto3->move('uploads', $name3);
			}else{
				$name3 = 'no-image.png';
			}

			helper('text');
			$kode_laporan = strtoupper(random_string('alnum', 8));
			$data = [
				'kode_laporanppa' => $kode_laporan,
				'nama_pelapor' => $this->request->getPost('nama_pelapor'),
				'ktp_pelapor' => $this->request->getPost('nik_pelapor'),
				'wa_pelapor' => $this->request->getPost('wa_pelapor'),
				'hp_pelapor' => $this->request->getPost('hp_pelapor'),
				'nama_korban' => $this->request->getPost('nama_korban'),
				'perkiraan_usia_korban' => $this->request->getPost('usia_korban'),
				'alamat_kejadian' => $this->request->getPost('alamat_kejadian'),
				'kronologi_kejadian' => $this->request->getPost('kronologi_kejadian'),
				'bukti_foto1' => $name1,
				'bukti_foto2' => $name2,
				'bukti_foto3' => $name3,
			];
			$this->laporanppa->save($data);
			return redirect()->to(base_url('/layananppa/result/'.$kode_laporan));
		}else{
			session()->setFlashdata('info', '<div class="alert alert-danger alert-icon alert-dismissible">
												<strong>Kirim Gagal</strong>! File Yang Diupload Harus JPG/JPEG/PNG !
											</div>');
			return redirect()->to(base_url('/layananppa/new'));
		}


	}
	public function result($kode){
		$laporan = $this->laporanppa->where('kode_laporanppa', $kode)
                   ->first();

        $data = [
        	'laporan' => $laporan
        ];
        return view('ppa/result', $data);
	}
	public function exportpdf($kode){
		$laporan = $this->laporanppa->where('kode_laporanppa', $kode)
                   ->first();

        $data = [
        	'laporan' => $laporan
        ];

		$dompdf = new \Dompdf\Dompdf(); 
        $dompdf->loadHtml(view('ppa/pdf',$data));
        $dompdf->setPaper('A4', 'portrait');
        $dompdf->render();
        $dompdf->stream($kode.".pdf");
	}
	public function cari($id = null){
		if(!empty($id)){
			$result = $this->laporanppa->where('id', $id)->first();
			$resultcatatan = $this->catatanlaporanppa->where('laporanppa_id', $id)->findAll();
		}else{
			$kode = $this->request->getPost('cari');
			$result_kode = $this->laporanppa->where('kode_laporanppa', $kode)
					   ->first();
			$result_ktp = $this->laporanppa->where('ktp_pelapor', $kode)
					   ->first();
	
			if(!empty($result_ktp)){
				$result = $result_ktp;
				$resultcatatan = $this->catatanlaporanppa->where('laporanppa_id', $result['id'])->findAll();
			}elseif(!empty($result_kode)){
				$result = $result_kode;
				$resultcatatan = $this->catatanlaporanppa->where('laporanppa_id', $result['id'])->findAll();
			}else{
				$result = 0;
			}
			
		}

		if($result != 0){
			$data = [
				'laporan' => $result,
				'catatan' => $resultcatatan
			];
			return view('ppa/cari', $data);
		}else{
			session()->setFlashdata('info', '<div class="alert alert-danger alert-icon alert-dismissible">
				<strong>Data Laporan Tidak Ditemukan</strong>! Mohon Masukkan Kembali Data Dengan Cermat!</button>
			</div>');
			return redirect()->to(base_url('layananppa/cek'));
		}
	}
	public function edit($id){
		$laporan = $this->laporanppa->where('id', $id)
                   ->first();
		if($this->request->getMethod() == 'post'){
			$foto1 = $this->request->getFile('foto1');
			$foto2 = $this->request->getFile('foto2');
			$foto3 = $this->request->getFile('foto3');
			$name1 = '';
			$name2 = '';
			$name3 = '';
			if($foto1->isValid()){
				$name1 = $foto1->getRandomName();
	           	$foto1->move('uploads', $name1);
				$fotolama1 = $this->laporanppa->find($id)['bukti_foto1'];
				if($fotolama1 != 'no-image.png'){
					$file_path1 = ROOTPATH.'/public/uploads/'.$fotolama1;
					unlink($file_path1);
				}
				
			}else{
				$name1 = $this->request->getPost('foto1lama');
			}

			if($foto2->isValid()){
				$name2 = $foto2->getRandomName();
	           	$foto2->move('uploads', $name2);
				$fotolama2 = $this->laporanppa->find($id)['bukti_foto2'];
				if($fotolama2 != 'no-image.png'){
					$file_path1 = ROOTPATH.'/public/uploads/'.$fotolama2;
					unlink($file_path1);
				}
			}else{
				$name2 = $this->request->getPost('foto2lama');
			}

			if($foto3->isValid()){
				$name3 = $foto3->getRandomName();
	           	$foto3->move('uploads', $name3);
				$fotolama3 = $this->laporanppa->find($id)['bukti_foto3'];
				if($fotolama3 != 'no-image.png'){
					$file_path1 = ROOTPATH.'/public/uploads/'.$fotolama3;
					unlink($file_path1);
				}
			}else{
				$name3 = $this->request->getPost('foto3lama');
			}
			$file = $this->request->getFile('foto1');
			$data = [
				'id' => $id,
				'nama_pelapor' => $this->request->getPost('nama_pelapor'),
				'ktp_pelapor' => $this->request->getPost('nik_pelapor'),
				'wa_pelapor' => $this->request->getPost('wa_pelapor'),
				'hp_pelapor' => $this->request->getPost('hp_pelapor'),
				'nama_korban' => $this->request->getPost('nama_korban'),
				'perkiraan_usia_korban' => $this->request->getPost('usia_korban'),
				'alamat_kejadian' => $this->request->getPost('alamat_kejadian'),
				'kronologi_kejadian' => $this->request->getPost('kronologi_kejadian'),
				'bukti_foto1' => $name1,
				'bukti_foto2' => $name2,
				'bukti_foto3' => $name3,
			];
			$this->laporanppa->save($data);
			
			return redirect()->to(base_url('layananppa/cari/'.$id.''));
		}else{
			$data = [
				'laporan' => $laporan
			];
			return view('ppa/edit', $data);
		}

        
	}
}
