<?php

namespace App\Controllers;
use App\Models\Layananumum_m;

class Layananumum extends BaseController
{
	public function __construct()
    {
        // parent::__construct();
        $this->layananumum = new layananumum_m();
    }
	public function index()
	{
		return view('umum/home');
	}
    public function laporkan($laporan)
	{
        $data = [
            'laporan' => $laporan
        ];
        
        if($this->request->getMethod() == 'post'){
            $kecamatan_tkp = $this->request->getPost('kec_tkp');
            $nama_pelapor = $this->request->getPost('nama_pelapor');
            $alamat_tkp = $this->request->getPost('alamat_tkp');
            $kategori = $this->request->getPost('kategori_aduan');
            $data = [
                'kategori_aduan' => $kategori,
                'nama_pelapor' => $nama_pelapor,
                'no_hp' => $this->request->getPost('no_hp'),
                'alamat_pelapor' => $this->request->getPost('alamat_pelapor'),
                'kecamatan_tkp' => $kecamatan_tkp,
                'alamat_tkp' => $alamat_tkp
            ];
            $this->layananumum->save($data);
            return redirect()->to(base_url('https://wa.me/62895335602756?text=Saya%20atas%20nama%20'.$nama_pelapor.',%20melaporkan%20'.$kategori.'%20di%20'.$alamat_tkp.'%20, %20'.$kecamatan_tkp.''));
        }
		return view('umum/laporkan',$data);
	}
	
}
