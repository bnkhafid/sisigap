<?php

namespace App\Controllers\Admin;
use App\Controllers\BaseController;
use App\Models\Laporanppa_m;
use App\Models\Laporanppacatatan_m;

class PPA extends BaseController
{
	public function __construct()
    {
		$this->laporanppa = new Laporanppa_m();
		$this->catatanlaporanppa = new Laporanppacatatan_m();
    }
	public function index()
	{
		$laporan = $this->laporanppa->findAll();

        $data = [
        	'laporan' => $laporan
        ];
        return view('admin/ppa',$data);
	}
	public function detail($id = null){
		$laporan = $this->laporanppa->find($id);
		$resultcatatan = $this->catatanlaporanppa->where('laporanppa_id', $id)->findAll();
        $data = [
        	'laporan' => $laporan,
			'pesan' => $resultcatatan
        ];
        return view('admin/ppadetail',$data);
	}
	public function kirim_pesan($id = null){
		$data = [
			'laporanppa_id' => $id,
			'sender' => 'Admin',
			'pesan' => $this->request->getPost('pesan')
		];
		if($this->catatanlaporanppa->insert($data)){
			return redirect()->to(base_url('/admin/ppa/detail/'.$id.''));
		}else{
			echo "gagal";
		}
		
	}
	public function update($id){
		$data = [
			'status' => $this->request->getPost('status'),
		];
		if($this->laporanppa->update($id, $data)){
			return redirect()->to(base_url('/admin/ppa/detail/'.$id.''));
		}else{
			echo "gagal";
		}
	}
	
	
	

}
