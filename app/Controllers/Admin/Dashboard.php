<?php

namespace App\Controllers\Admin;
use App\Controllers\BaseController;

class Dashboard extends BaseController
{
	public function index()
	{
        if(empty(session()->get('administrator'))){
            return redirect()->to(base_url('/admin/login'));
        }else{
            $this->db = \Config\Database::connect();
            $data = [
                'kategori' => $this->db->table('tblkategori')->countAll(),
                'menu' => $this->db->table('tblmenu')->countAll(),
                'pelanggan' => $this->db->table('tblpelanggan')->countAll(),
                'order' => $this->db->table('tblorder')->countAll()
            ];
            
            return view('dashboard_admin', $data);
        }
	}
	
	

}
