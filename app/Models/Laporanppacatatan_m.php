<?php

namespace App\Models;

use CodeIgniter\Model;

class Laporanppacatatan_m extends Model
{
    protected $table = 'catatanlaporanppa';
    protected $primaryKey = 'id';

    protected $allowedFields = ['laporanppa_id','sender','pesan'];
}