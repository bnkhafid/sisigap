<?php

namespace App\Models;

use CodeIgniter\Model;

class Layananumum_m extends Model
{
    protected $table = 'layananumum';
    protected $primaryKey = 'id';

    protected $allowedFields = ['kategori_aduan', 'nama_pelapor','no_hp', 'alamat_pelapor', 'kecamatan_tkp', 'alamat_tkp'];
}