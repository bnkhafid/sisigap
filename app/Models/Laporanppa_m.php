<?php

namespace App\Models;

use CodeIgniter\Model;

class Laporanppa_m extends Model
{
    protected $table = 'laporanppa';
    protected $primaryKey = 'id';

    protected $allowedFields = ['kode_laporanppa', 'nama_pelapor', 'ktp_pelapor', 'wa_pelapor', 'hp_pelapor', 'nama_korban', 'perkiraan_usia_korban', 'alamat_kejadian', 'kronologi_kejadian', 'bukti_foto1','bukti_foto2','bukti_foto3','status'];
}